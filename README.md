# Packaging Syncthing Tray

To clone this repository run:

    git clone git@salsa.debian.org:rittich/packaging-syncthingtray.git packaging-syncthingtray
    cd packaging-syncthingtray
    git submodule update --init

Running

    gbp buildpackage

in each of the folders should build the needed packages.
